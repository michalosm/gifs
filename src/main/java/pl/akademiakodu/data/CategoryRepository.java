package pl.akademiakodu.data;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import pl.akademiakodu.model.Category;
import pl.akademiakodu.model.Gif;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by michalos on 13.10.2016.
 */

@Component
public class CategoryRepository {
    private static final List<Category> ALL_CATEGORIES = Arrays.asList(
            new Category(1,"Osoby"),
            new Category(2,"Sport"),
            new Category(3,"Memy")
    );

    public List<Category> getAllCategories(){
        return ALL_CATEGORIES;
    }

    public Category findById(int id){
        for (Category category:ALL_CATEGORIES){
            if (category.getId()==id){
                return category;
            }
        }
        return null;
    }



}

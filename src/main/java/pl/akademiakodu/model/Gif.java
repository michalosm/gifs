package pl.akademiakodu.model;

/**
 * Created by michalos on 12.10.2016.
 */
public class Gif {
    private String name;

    private String username;

    private boolean favorite;

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    private int categoryId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public Gif(String name,int categoryId, String username, boolean favorite) {
        this.name = name;
        this.categoryId = categoryId;
        this.username = username;
        this.favorite = favorite;
    }


}

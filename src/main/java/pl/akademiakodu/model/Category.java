package pl.akademiakodu.model;

/**
 * Created by michalos on 13.10.2016.
 */
public class Category {
    private String name;
    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Category(int id, String name) {
        this.name = name;
        this.id = id;
    }
}
